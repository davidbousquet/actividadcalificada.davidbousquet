<%-- 
    Document   : index
    Created on : Mar 28, 2021, 1:15:06 PM
    Author     : davidbousquet

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actividad calificada - EJE01</title>
    </head>
    <body>
        <h1>Proyecto Java JSP en Bitbucket </h1>
        <p>
            Estudiante: David Bousquet
            Asignatura: TALLER DE APLICACIONES EMPRESARIALES - IC201IECIREOL

            <%
                out.println("Tu dirección IP es " + request.getRemoteAddr());
                out.println(request.getLocalName());
            %>
        </p>
    </body>
</html>
